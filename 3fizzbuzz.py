#!/usr/bin/python

# User selected variables
while True:
	try:
		output = int(input('Choose wether to print results on individual lines or as a single-line list.\n(Lines = 1, List = 2):'))
		if output < 0 or output > 2:
			raise ValueError
		break
	except ValueError:
		print('Please enter a "1" to print line by line or "2" to print a list')

start = int(input('Enter first number of the range (as an integer):\n'))
end = int(input('Enter last number of the range (as an integer):\n'))

# print lines
if output == 1:
	for x in range(start,(end+1)):
		if x %3 == 0 and x %5 == 0:
			print('FizzBuzz')
		elif x %3 == 0:
			print('Fizz')
		elif  x %5 == 0:
			print('Buzz')
		else:
			print(x)

# print list
if output == 2:
	list = [i for i in range(start,end+1)]
	for i in list:
		if i %3 == 0 and i %5 == 0:
			y = list.index(i)
			list.pop(y)
			list.insert(y,"FizzBuzz")
		elif i %3 == 0:
			y = list.index(i)
			list.pop(y)
			list.insert(y,"Fizz")
		elif i %5 == 0:
			y = list.index(i)
			list.pop(y)
			list.insert(y,"Buzz")
	print(list)
